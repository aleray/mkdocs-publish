import mkdocs
import os


class PublishPlugin(mkdocs.plugins.BasePlugin):
    config_scheme = (
        ("enabled", mkdocs.config.config_options.Type(bool, default=True)),
    )

    def on_config(self, config, **kwargs):
        # Add the "publish" flag to the global config
        config.extra["publish"] = self.config.get("enabled")
        return config

    def on_pre_page(self, page, config, files, **kwargs):
        # Set a flag indicating whether the page is an index page
        path = page.file.src_path
        page.meta["is_index"] = os.path.basename(path) == "index.md"
        return page

    def on_post_page(self, output, page, config, **kwargs):
        # Check if the "publish" flag is set for the page
        if self.config.get("enabled") and (
            "publish" not in page.meta or not page.meta["publish"]
        ):
            # If not published, return an empty string to exclude it from the output
            return ""

        # If published, continue with the default output
        return output
