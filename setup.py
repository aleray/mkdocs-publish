from setuptools import setup, find_packages

setup(
    name='mkdocs_publish',
    version='0.0.1',
    description='MkDocs plugin to add a "publish" flag to pages',
    url='https://tangible-cloud.be/',
    author='Alex',
    author_email='alexandre@stdin.fr',
    packages=find_packages(),
    install_requires=[
        'mkdocs>=1.0',
    ],
    entry_points={
        'mkdocs.plugins': [
            'publish = mkdocs_publish.plugin:PublishPlugin',
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
)
