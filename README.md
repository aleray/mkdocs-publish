# MkDocs Publish Plugin

This MkDocs plugin adds a "publish" flag to pages, allowing you to control which pages are included in the generated output. If a page is marked as unpublished, it will be excluded from the final documentation.

## Installation

Install the plugin using pip:

```bash
pip install mkdocs-publish-plugin
```

Then, add the following lines to your `mkdocs.yml`:

```yaml
plugins:
  - publish:
      enabled: true  # Set to false to exclude unpublished pages
```

## Usage

### Configuring Pages

To mark a page as unpublished, you can add the following metadata to the page's YAML front matter:

```yaml
publish: false
```

By default, all pages are considered published unless the global `enabled` configuration is set to `false`.

### Special Handling for Index Page

The plugin also sets a "is_index" flag for each page, indicating whether the page is the index page (`index.md`). This can be useful for customizing the output based on whether a page is the index.

## Example

```yaml
# mkdocs.yml

plugins:
  - publish:
      enabled: true
```

```markdown
# index.md

---
title: Home
publish: true
---

Welcome to the documentation!
```

```markdown
# guide.md

---
title: User Guide
publish: false
---

This page will not be included in the generated output.
```

## License

License info yet to come
